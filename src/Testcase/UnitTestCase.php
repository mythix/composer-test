<?php
namespace Essent\Test\Testcase;

if (!class_exists('UnitTester')) {
    throw new \Exception('The UnitTester class should exist (check the tests/_support folder)');
}

use Codeception\TestCase\Test;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use UnitTester;

class UnitTestCase extends Test
{
    use MockeryPHPUnitIntegration;

    /** @var UnitTester */
    protected $tester;
}
