<?php
namespace Essent\Test\Testcase;

if (!class_exists('FunctionalTester')) {
    throw new \Exception('The FunctionalTester class should exist (check the tests/_support folder)');
}

use Codeception\TestCase\Test;
use FunctionalTester;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

class FunctionalTestCase extends Test
{
    use MockeryPHPUnitIntegration;

    /** @var FunctionalTester */
    protected $tester;

    /**
     * @param string $location
     * @param string $name
     * @return array
     */
    public function getResources($location, $name)
    {
        $data = [];

        $testFiles = glob($location . $name . '*.json');
        if (!empty($testFiles)) {
            foreach ($testFiles as $file) {
                $testValues = json_decode(file_get_contents($file));
                preg_match('/' . $name . '(.*?).json/', $file, $match);
                $key = $match[1] . (!empty($testValues->_description) ? ' - ' . $testValues->_description : '');
                $data[$key] = [$testValues];
            }
        }

        return $data;
    }
}
