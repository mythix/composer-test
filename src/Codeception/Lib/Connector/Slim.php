<?php
namespace Essent\Test\Codeception\Lib\Connector;

use Slim\App;
use Slim\Http\Environment;
use Slim\Http\Headers;
use Slim\Http\Request as SlimRequest;
use Slim\Http\Response as SlimResponse;
use Slim\Http\Stream;
use Slim\Http\Uri;
use Slim\Route;
use Slim\Router;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\BrowserKit\Response;

class Slim extends Client
{
    /** @var App */
    protected $application;

    /** @var string */
    protected $basePath;

    /**
     * @param App $application
     */
    public function setApplication(App $application)
    {
        $this->application = $application;
    }

    /**
     * @param string $basePath
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
    }

    /**
     * Makes a request.
     *
     * @param Request $request
     *
     * @return Response
     */
    protected function doRequest($request)
    {
        /** @var Request $request */
        $inputStream = fopen('php://memory', 'rb+');
        $content = $request->getContent();
        if ($content !== null) {
            fwrite($inputStream, $content);
            rewind($inputStream);
        }
        $body = new Stream($inputStream);

        $uri = Uri::createFromString($request->getUri());
        $uri = $uri->withPath(substr($uri->getPath(), strlen($this->basePath)));
        $uri = $uri->withBasePath($this->basePath);

        $queryParams = [];
        $postParams = [];
        if ($uri->getQuery() != '') {
            parse_str($uri->getQuery(), $queryParams);
        }
        if ($request->getMethod() !== 'GET') {
            $postParams = $request->getParameters();
        }

        $headers = Headers::createFromEnvironment(new Environment());
        $serverParams = $request->getServer();
        $serverParams['REMOTE_ADDR'] = '127.0.0.1';
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

        $request = new SlimRequest(
            $request->getMethod(),
            $uri,
            $headers,
            $request->getCookies(),
            $serverParams,
            $body,
            $request->getFiles()
        );
        $request = $request
            ->withQueryParams($queryParams)
            ->withParsedBody($postParams)
        ;

        $response = $this->application->process($request, new SlimResponse());

        $this->resetRouteArguments();

        return new Response(
            $response->getBody(),
            $response->getStatusCode(),
            $response->getHeaders()
        );
    }

    /**
     *
     */
    protected function resetRouteArguments()
    {
        /** @var Router $router */
        $router = $this->application->getContainer()->get('router');

        /** @var Route $route */
        foreach ($router->getRoutes() as $route) {
            $route->setArguments([]);
        }
    }
}