Http Request Recording module
-----------------------------

Adds recording and/or playback of http requests, so the build server does not actually call external services.

Add the config of the module to codeception.yml in your project:

```
modules:
    config:
        Essent\Test\Codeception\Module\Vcr:
            cassettePath: 'tests/_vcr'
            record: true
            playback: false
```

* cassettePath: path to save the recordings
* record: whether to record http requests (should be true in vagrant, all other environments false)
* playback: whether to playback http request recordings (use false in vagrant, all other environments true)

And add the module to the config yml of your suite SUITENAME.suite.yml:

```
modules:
    enabled:
        - Essent\Test\Codeception\Module\Vcr
```

The module should not be used in the UNIT suite (since they should mock http requests anyway). 